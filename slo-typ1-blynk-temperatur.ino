/***slo-typ1-blynk-temperatur
Als Vorlage wurde der Sketch aus http://www.instructables.com/id/Blynk-Arduino-DS18B20-Thermometer-Display-on-IOS-o/?ALLSTEPS
benutzt und für den NodeMCU 1.0 und DS18B20 von @hosi1709, SLO umgeändert
09.2016***/

#define BLYNK_PRINT Serial // startet den Serial Monitor
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <OneWire.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
//#include <WiFiManager.h>

OneWire  ds(2);  // an  pin 2, D4 in NodeMCU, (ein 4.7K resistor ist eventuell notwendig)


char auth[] = "dein auth-token";   // Name des Auth Token 
char ssid[] = "dein wlan";    //WlAN Name
char pass[] = "dein wlan-pw"; //WLAN Passwort

int sensorData;
BlynkTimer timer;

void myTimerEvent()
{
  sensorData = analogRead(0);
  Blynk.virtualWrite(V4, sensorData);
}

void setup()
{
  Serial.begin(9600);
//  WiFiManager wifiManager;
//  wifiManager.autoConnect("AutoConnectAP"); 
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(1000L, myTimerEvent);  
}

void loop()
{
  Blynk.run(); 
  timer.run();
  
//  delay(10000);
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;

  if ( !ds.search(addr)) {
    Serial.println("keine weiteren Adressen");
    Serial.println();
    ds.reset_search();
    delay(250);
    return;
  }

  Serial.print("ROM =");
  for ( i = 0; i < 8; i++) {
    Serial.write(' ');
    Serial.print(addr[i], HEX);
  }

  if (OneWire::crc8(addr, 7) != addr[7]) {
    Serial.println("CRC is not valid!");
    return;
  }
  Serial.println();

 
  switch (addr[0]) {
    case 0x10:
      Serial.println("  Chip = DS18S20");  
      type_s = 1;
      break;
    case 0x28:
      Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
      Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
      Serial.println("Gerät gehört nicht zur Famile der  DS18x20 ");
      return;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        
  delay(1000);     
 

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         

  Serial.print("  Data = ");
  Serial.print(present, HEX);
  Serial.print(" ");
  for ( i = 0; i < 9; i++) {          
    data[i] = ds.read();
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
  Serial.print(" CRC=");
  Serial.print(OneWire::crc8(data, 8), HEX);
  Serial.println();

  
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; 
    if (data[7] == 0x10) {
     
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    
    if (cfg == 0x00) raw = raw & ~7;  // 
    else if (cfg == 0x20) raw = raw & ~3; 
    else if (cfg == 0x40) raw = raw & ~1; 
   
  }
  celsius = (float)raw / 16.0;

  Serial.print("  Temperature = ");
  Serial.print(celsius);
  Serial.print(" Celsius, ");

 
  Blynk.virtualWrite(V5, celsius);
 
}




