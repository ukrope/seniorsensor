# SeniorSensor

Der SeniorSensor ist  eine Idee von Senioren-Lernen-Online, private 
Netzwerke für Senioren aufzubauen, in denen mit Sensoren und W-LAN fähigen Mikrocontrollern
Älteren und deren Betreuern ein wenig Sicherheit gegeben werden soll,
wenn Senioren allein zuhause leben.
In dem eBook (https://sloiot.pressbooks.com/) findet man die
Bauanleitung und die einzelnen Programme (siehe dort im Anhang).
In den dort aufgeführten Programmen müssen die Eingabedaten für W-LAN 
und die Daten für die Plattformen Thingspeak oder Blynk in dem Programm 
eingesetzt werden, bevor sie kompiliert werden.
Die Datei slo-typ1-thingspeak-temperatur.ino und slo-typ2- thingspeak-bewegung 
starten mit einer  Benutzeroberfläche, in der die Eingabedaten
angegeben werden und das Programm nicht kompiliert werden muss.
( Ebenso werden die Programme für Blynk, um diese Komponente erweitert,demnächst hochgeladen)
Nachteil,bei jedem Stromausfall muss neu eingegeben werden.
Anleitung für die Eingabe der Daten findet man in der  pdf Datei.
Es wurde mit arduino1.8.5. gearbeitet, alle Bibliotheken Stand 2018.