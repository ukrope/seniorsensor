/***slo-typ2-blynk-bewegung-v2
Als Vorlage wurde der Sketch aus http://www.instructables.com/id/Blynk-Arduino-DS18B20-Thermometer-Display-on-IOS-o/?ALLSTEPS
benutzt und für den NodeMCU 1.0 und HC-SR 501 über Blynk von @hosi1709, SLO umgeändert
02.2018***/

#define BLYNK_PRINT Serial // startet den Serial Monitor
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>

char auth[] = "Ihr Auth Token aus Blynk";   //Name des Auth Token 
char ssid[] = "Ihr WLAN Name";    //WlAN Name
char pass[] = "Ihr WLAN Passwort"; //WLAN Passwort

int sensorData;

BlynkTimer timer;

void myTimerEvent()
{
  sensorData = analogRead(0);         //Outputpin Sensor auf Pin A0 am NodeMCU
  Blynk.virtualWrite(V4, sensorData); //Widgets in der App Blynk auf Pin V4 und Push
}

void setup()

{
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(1000L, myTimerEvent);   
}

void loop()
{
  Blynk.run(); 
  timer.run();
   
}




